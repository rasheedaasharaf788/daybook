/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.daybook.service.Impl;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.cj.protocol.Resultset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 *
 * @author student
 */
public  class AccountdetailsServiceImpl extends ConnectionServiceImpl  {
 

    public String saveAccountdetails(String Date, String AccountName,int debitAmount,int creditAmount) {
        try {
   
    
        Connection connection = getConnection();
        
        Statement statement = connection.createStatement();
        String query = "INSERT INTO ACCOUNTDETAILS (DATE,ACCOUNT_NAME,DEBIT_AMOUNT,CREDIT_AMOUNT) VALUES"
                + "(STR_TO_DATE('" + Date + "','%d/%m/%Y %H:%i:%s'),'" + AccountName + "',"+debitAmount+","+creditAmount+")";
        System.err.println("Query:" + query);
        int status = statement.executeUpdate(query);
        if (status !=1) {
            return "Save failed";
            
        } else {
            return "Save successfully";
        }
        }catch (SQLException ex) {
            System.out.println(ex);
            return "Save failed";
        }
    }    

    
    public Accountdetails readAccountdetails(Integer id) throws SQLException {
        //
    
        try {
            Accountdetails accountdetails = null;
               Connection connection = getConnection1();
               Statement statement = connection.createStatement();
             String query ="SELECT * FROM ACCOUNTDETAILS WHERE ID = "+id;
            Resultset resultSet = (Resultset) statement.executeQuery(query);
             while(((ResultSet) resultSet).next()){
                 id = ((ResultSet) resultSet).getInt("ID");
                 String date = ((ResultSet) resultSet).getString("DATE");
                 String accountname = ((ResultSet) resultSet).getString("ACCOUNT_NAME");
                 accountdetails = new Accountdetails(id,date,accountname);
                 //Accountdetails.add(Accountdetails);
             }
             return accountdetails;
        } catch (SQLException ex) {
            Logger.getLogger(AccountdetailsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
        }
		return null;
        
        
    }

        
         
          public ResultSet getAlldetailsByColomnName(String Colomn) {
        
        ResultSet resultSet = null;
          
          try {
              Connection connection = getConnection();
           Statement statement = connection.createStatement();
        String query ="SELECT "+Colomn+ " FROM ACCOUNTDETAILS";
        
        resultSet = statement.executeQuery(query);
        
          } catch (SQLException ex) {
            Logger.getLogger(AccountdetailsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
          }
		return resultSet;
    }
         
              public boolean UpdateAccountdetails(int id, String date1,String accountname,int debitamount,int creditamount) throws ParseException {
        //
        try {
            Connection connection = getConnection();
                 
           Statement statement = connection.createStatement();
            
           
   
   
           String query = "UPDATE ACCOUNTDETAILS SET DATE='"+date1+"', ACCOUNT_NAME='"+accountname+"', DEBIT_AMOUNT= "+debitamount+", CREDIT_AMOUNT="+creditamount+" WHERE ID="+id;
           System.out.print(query);
           int update = 0;
		try {
			update = statement.executeUpdate(query);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
               
           if(update==1)
           {
               return true;
           }
           else
           {
               return false;
           }
        }
           catch(SQLException ex){
                   return false;
                   }
           
   
        }
              
                 
             
             
        
        

public Connection getConnection1() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public void getAllAccountdatas() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Boolean DeleteAccount(Integer id) {
        try
        {
        Connection connection = getConnection();
           Statement statement = connection.createStatement();
           String query = "DELETE FROM ACCOUNTDETAILS WHERE ID="+id;
           statement.execute(query);
           return true;
        }
        catch(SQLException e)
        {
            System.out.println(e);
            return false;
        }
         
}

    public String[] getAllAccount() throws SQLException {
         Connection connection = getConnection();
               Statement statement = null;
               ArrayList<String> id =new ArrayList<String>();
        try {
            statement = connection.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(AccountdetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
             String query ="SELECT * FROM accountdetails";
           ResultSet resultSet = (ResultSet) (Resultset) statement.executeQuery(query);
           int i=0;
           while(resultSet.next())
           {
                id.add(String.valueOf(resultSet.getInt("ID")));
                System.out.println(String.valueOf(resultSet.getInt("ID")));
                i++;
           }
           
           String[] idArr = new String[id.size()];
idArr = id.toArray(idArr);
           return idArr;          
        }
}


    
