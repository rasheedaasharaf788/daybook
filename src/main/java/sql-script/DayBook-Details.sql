-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2022 at 03:12 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `daybookdetails`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountdetails`
--

CREATE TABLE `accountdetails` (
  `ID` int(11) NOT NULL,
  `DATE` timestamp NULL DEFAULT NULL,
  `ACCOUNT_NAME` varchar(20) NOT NULL,
  `DEBIT_AMOUNT` int(11) NOT NULL,
  `CREDIT_AMOUNT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accountdetails`
--

INSERT INTO `accountdetails` (`ID`, `DATE`, `ACCOUNT_NAME`, `DEBIT_AMOUNT`, `CREDIT_AMOUNT`) VALUES
(9, '2023-05-17 20:00:00', 'Rashee New', 1002, 101),
(12, '2022-02-02 20:00:00', '255', 25, 30);

-- --------------------------------------------------------

--
-- Table structure for table `day_book_details`
--

CREATE TABLE `day_book_details` (
  `ID` int(11) NOT NULL,
  `ACCOUNT_TYPE` varchar(20) NOT NULL,
  `DATE` date DEFAULT NULL,
  `CREDIT_AMOUNT` decimal(8,2) NOT NULL,
  `DEBIT_AMOUNT` decimal(8,2) NOT NULL,
  `ACCOUNT_ID` int(11) DEFAULT NULL,
  `TYPE` varchar(50) NOT NULL,
  `BALANCE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `day_book_details`
--

INSERT INTO `day_book_details` (`ID`, `ACCOUNT_TYPE`, `DATE`, `CREDIT_AMOUNT`, `DEBIT_AMOUNT`, `ACCOUNT_ID`, `TYPE`, `BALANCE`) VALUES
(13, 'General', '2022-12-20', '30.00', '20.00', 9, 'OP', 50);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accountdetails`
--
ALTER TABLE `accountdetails`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `day_book_details`
--
ALTER TABLE `day_book_details`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ACCOUNT_ID` (`ACCOUNT_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accountdetails`
--
ALTER TABLE `accountdetails`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `day_book_details`
--
ALTER TABLE `day_book_details`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `day_book_details`
--
ALTER TABLE `day_book_details`
  ADD CONSTRAINT `day_book_details_ibfk_1` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `accountdetails` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
